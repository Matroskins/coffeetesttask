import { TCardPayStep } from "./types";

export class Emulator {
  private cashListenerHandler?: (event: KeyboardEvent) => void;
  private cardListenerHandler?: (event: KeyboardEvent) => void;
  private vendListenerHandler?: (event: KeyboardEvent) => void;

  StartCashin(addCash: (amount: number) => void) {
    this.cashListenerHandler = (event) => {
      if (event.code === "KeyF") {
        addCash(50);
      } else if (event.code === "KeyH") {
        addCash(100);
      } else if (event.code === "KeyT") {
        addCash(10);
      }
    };

    document.addEventListener("keydown", this.cashListenerHandler);
  }

  StopCashin() {
    if (this.cashListenerHandler) {
      document.removeEventListener("keydown", this.cashListenerHandler);
    } else {
      console.log(
        "Невозможно отключить прием валюты - потому что он не был включен",
      );
    }
  }
  BankCardPurchase({
    amount,
    cb,
    display_cb,
  }: {
    amount: number;
    cb: (isTransitionRight: boolean) => void;
    display_cb: (step: TCardPayStep) => void;
  }) {
    this.cardListenerHandler = (event) => {
      if (event.code === "KeyY") {
        alert(`Оплачено ${amount}  \u20bd`);
        cb(true);
      } else if (event.code === "KeyN") {
        cb(false);
      } else if (event.code === "KeyS") {
        display_cb("Приложите карту");
      } else if (event.code === "KeyP") {
        display_cb("Обработка карты");
      } else if (event.code === "KeyC") {
        display_cb("Связь c банком");
      }
    };
    document.addEventListener("keydown", this.cardListenerHandler);
  }
  BankCardCancel() {
    if (this.cardListenerHandler) {
      document.removeEventListener("keydown", this.cardListenerHandler);
    } else {
      console.log(
        "Невозможно отключить операции с банковской картой - потому что они не были включены",
      );
    }
  }
  Vend(product_idx: number, cb: (isCoffeeReady: boolean) => void) {
    this.vendListenerHandler = (event) => {
      if (event.code === "KeyY") {
        cb(true);
      } else if (event.code === "KeyN") {
        cb(false);
      }
    };
    document.addEventListener("keydown", this.vendListenerHandler);
  }
  VendCancel() {
    if (this.vendListenerHandler) {
      document.removeEventListener("keydown", this.vendListenerHandler);
    } else {
      console.log(
        "Невозможно отключить операции выдачи напитка - потому что они не были включены",
      );
    }
  }
}
