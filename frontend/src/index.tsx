import React from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Modal from "react-modal";
import "./index.css";
import {
  DrinksList,
  CardPayment,
  CoffeeReady,
  CoffeeCreating,
  CashPayment,
} from "./components";
const router = createBrowserRouter([
  {
    path: "/",
    element: <DrinksList />,
  },
  {
    path: "cashPayment/:id",
    element: <CashPayment />,
  },
  {
    path: "cardPayment/:id",
    element: <CardPayment />,
  },
  {
    path: "coffeeCreating/:id",
    element: <CoffeeCreating />,
  },
  {
    path: "coffeeReady/:id",
    element: <CoffeeReady />,
  },
]);

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement,
);

Modal.setAppElement("#root");

root.render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
);
