export const mockData = {
  drinksList: [
    {
      id: 100,
      name: "Эспрессо",
      price: 79,
      imgName: "espresso",
    },
    {
      id: 101,
      name: "Американо",
      price: 119,
      imgName: "americano",
    },
    {
      id: 102,
      name: "Эспрессо",
      price: 109,
      imgName: "doppio",
    },
    {
      id: 104,
      name: "Латте",
      price: 129,
      imgName: "latte",
    },
    {
      id: 103,
      name: "Капучино",
      price: 129,
      imgName: "cappucino",
    },
    {
      id: 105,
      name: "Макиато",
      price: 129,
      imgName: "mokiato",
    },
  ],
};
