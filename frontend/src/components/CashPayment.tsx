import React from "react";
import { useParams, useNavigate } from "react-router-dom";
import { mockData } from "../data";
import { useEffect, useMemo, useState } from "react";
import { Emulator } from "../emulator";
import { CashPaymentContainer, Title2, InstructionText, ControlsContainer, Button } from './styles';


export const CashPayment = () => {
  const { id } = useParams();
  const navigate = useNavigate();

  const selectedDrink = useMemo(
    () => mockData.drinksList.find((drink) => drink.id === Number(id)),
    [id],
  );
  const [addedCash, setAddedCash] = useState<number>(0);
  const theDrinkIsPaidFor = selectedDrink
    ? selectedDrink.price - addedCash <= 0
    : false;
  const onStartCreatingCoffee = () => {
    navigate(`/coffeeCreating/${id}`);
  };
  const handleCancelPayment = () => {
    navigate('/')
  }
  useEffect(() => {
    const addCash = (amount: number) => {
      setAddedCash((prevCash) => prevCash + amount);
    };
    const emulator = new Emulator();
    emulator.StartCashin(addCash);
    return () => {
      emulator.StopCashin();
    };
  }, [selectedDrink]);
  return (
    <CashPaymentContainer>
      <div>
        <Title2>{`Пожалуйста, внесите ${selectedDrink?.price} \u20bd`}</Title2>
        <Title2>{`Вы внесли ${addedCash} \u20bd`}</Title2>
      </div>
      <div>
        <ul>
          <InstructionText>Нажмите на F, чтобы внести 50 рублей</InstructionText>
          <InstructionText>Нажмите на H, чтобы внести 100 рублей</InstructionText>
          <InstructionText>Нажмите на T, чтобы внести 10 рублей</InstructionText>
        </ul>
      </div>
      <ControlsContainer>
        <Button onClick={handleCancelPayment}>
          Отменить оплату
        </Button>
        <Button onClick={onStartCreatingCoffee} disabled={!theDrinkIsPaidFor}>
          Купить напиток
        </Button>
      </ControlsContainer>
    </CashPaymentContainer>
  );
};