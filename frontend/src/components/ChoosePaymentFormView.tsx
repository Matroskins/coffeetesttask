import React from "react";
import leftVector from '../images/vectorLeft.svg'
import currency from '../images/currency.svg'

import {
  ChoosePaymentContainer,
  ControlsContainer,
  CloseModalButton,
  CenterContainer,
  Button,
  ChoosePaymentDescriptionCurrency,
  Title2
} from './styles'
import { TChoosePaymentFormView } from '../types'


export const ChoosePaymentFormView = (props: TChoosePaymentFormView) => {
  const onSelectPayFormCard = () => {
    props.onPay("CARD");
  };
  const onSelectPayFormCash = () => {
    props.onPay("CASH");
  };


  return (
    <ChoosePaymentContainer>
      <CloseModalButton onClick={props.onCloseModal}><img alt='закрыть' src={leftVector} /></CloseModalButton>
      <CenterContainer>
        <ChoosePaymentDescriptionCurrency>
          <img alt='валюта' src={currency} />
        </ChoosePaymentDescriptionCurrency>
        <Title2 center>Выберите способ оплаты</Title2>
      </CenterContainer>
      <ControlsContainer>
        <Button onClick={onSelectPayFormCash}>Оплатить наличкой</Button>
        <Button onClick={onSelectPayFormCard}>Оплатить картой</Button>
      </ControlsContainer>
    </ChoosePaymentContainer>
  );
};
