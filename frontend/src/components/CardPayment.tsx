import React, { useMemo, useEffect } from "react";
import { Emulator } from "../emulator";
import { useParams, useNavigate } from "react-router-dom";
import { mockData } from "../data";
import { TCardPayStep } from "../types";
import { Title2, InstructionText, Button } from './styles'

export const CardPayment = () => {
  const { id } = useParams();
  const navigate = useNavigate();

  const selectedDrink = useMemo(
    () => mockData.drinksList.find((drink) => drink.id === Number(id)),
    [id],
  );
  const onReturn = () => {
    navigate("/");
  };

  useEffect(() => {
    const onCardTransaction = (isTransactionCorrect: boolean) => {
      if (isTransactionCorrect) {
        navigate(`/coffeeCreating/${id}`);
      } else {
        alert('Транзакция совершена с ошибкой!');
      }
    };
    const onDisplayScreen = (cardPayStep: TCardPayStep) => {
      switch (cardPayStep) {
        case "Обработка карты":
          alert("Обработка карты");
          break;
        case "Связь c банком":
          alert("Связь c банком");
          break;
        default:
          alert("Приложите карту");
          break;
      }
    };
    const emulator = new Emulator();
    if (selectedDrink?.price) {
      emulator.BankCardPurchase({
        amount: selectedDrink?.price,
        cb: onCardTransaction,
        display_cb: onDisplayScreen,
      });
    } else {
      alert('Транзакция совершена с ошибкой!');
    }
    return () => {
      emulator.BankCardCancel();
    };
  }, [selectedDrink, navigate, id]);
  return (
    <div>
      <Title2>Оплатите банковской картой</Title2>

      <ul>
        <InstructionText>Чтобы эмулировать успешную оплату банковской картой - нажмите Y</InstructionText>
        <InstructionText>Чтобы эмулировать ошибку при оплате банковской картой - нажмите N</InstructionText>
        <InstructionText>Чтобы эмулировать надпись от пип-пада "Приложите карту" - нажмите S </InstructionText>
        <InstructionText>Чтобы эмулировать надпись от пип-пада "Обработка карты" - нажмите P</InstructionText>
        <InstructionText>Чтобы эмулировать надпись от пип-пада "Связь c банком" - нажмите C</InstructionText>
      </ul>
      <Button onClick={onReturn}>Отмена оплаты</Button>
    </div>
  );
};
