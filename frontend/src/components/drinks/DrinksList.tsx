import React, { useCallback, useState } from "react";
import { mockData } from "../../data";
import { DrinkView } from "./DrinkView";
import { ChoosePaymentFormView } from "..";
import Modal from "react-modal";
import { useNavigate } from "react-router-dom";
import { DrinkListContainer, DrinksHeaderTitle, DrinksListContainer, DrinksContainer, Title2 } from '../styles'

export const DrinksList = () => {

  const [selectedDrinkId, setSelectedDrinkId] = useState<number | null>(null);
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
  const navigate = useNavigate();

  const handleSelectDrink = useCallback(
    (id: number) => {
      setSelectedDrinkId(id);
      setIsModalOpen(true);
    },
    [setIsModalOpen, setSelectedDrinkId],
  );
  const handleCloseModal = () => {
    setIsModalOpen(false);
  };
  const handlePay = (payType: "CASH" | "CARD") => {
    if (payType === "CASH") {
      navigate(`cashPayment/${selectedDrinkId}`);
    }
    if (payType === "CARD") {
      navigate(`cardPayment/${selectedDrinkId}`);
    }
    handleCloseModal();
  };

  return (
    <>
      <DrinksContainer>
        <DrinksHeaderTitle>Выбор напитка</DrinksHeaderTitle>
        <DrinksListContainer>
          <div>
            <Title2>Кофе</Title2>
          </div>
          <DrinkListContainer>
            {mockData.drinksList.map((drink) => (
              <DrinkView
                onSelectDrink={handleSelectDrink}
                key={drink.id}
                name={drink.name}
                price={drink.price}
                imgName={drink.imgName}
                id={drink.id}
              />
            ))}
          </DrinkListContainer>
        </DrinksListContainer>
      </DrinksContainer>
      <Modal style={{
        overlay: {
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center'

        },
        content: {
          position: 'static',
          maxWidth: 800,
          maxHeight: 1980,
          minWidth: 280,
          width: '72%',
          height: '90%',
        }
      }} onRequestClose={handleCloseModal} isOpen={isModalOpen}>
        <ChoosePaymentFormView onCloseModal={handleCloseModal} onPay={handlePay} />
      </Modal>
    </>
  );
};
