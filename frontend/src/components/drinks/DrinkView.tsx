import React, { memo } from "react";
import { DrinkContainer, DrinkName, DrinkPrice, DrinkDescriptionText } from "../styles";
import { TDrinkView } from "../../types";
import { DrinkImage } from './DrinkImage'

export const DrinkView = memo((props: TDrinkView) => {

  const handleSelectDrink = () => {
    props.onSelectDrink(props.id);
  };

  return (
    <DrinkContainer onClick={handleSelectDrink}>
      <DrinkImage name={props.name} imgName={props.imgName} />
      <DrinkDescriptionText>
        <DrinkName>{`${props.name}`}</DrinkName>
        <div><span>от </span><DrinkPrice>{`${props.price} \u20bd`}</DrinkPrice>
        </div>
      </DrinkDescriptionText>
    </DrinkContainer>
  );
});
