import React, { useState } from 'react'
import { TDrinkImage } from '../../types'


export const DrinkImage = (props: TDrinkImage) => {

  const [drinkImage, setDrinkImage] = useState();

  import(`../../images/${props.imgName}.svg`).then((image) =>
    setDrinkImage(image.default),
  );

  return (
    <div><img alt={props.name} src={drinkImage} /></div>
  )
}