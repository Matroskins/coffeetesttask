import React from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useTimer } from "use-timer";
import ProgressBar from "@ramonak/react-progress-bar";
import { Button, CenterContainer, CoffeeCreatingProgressBarContainer, CoffeeCreatingText } from './styles'

const CREATING_TIME = 20;

export const CoffeeCreating = () => {

  const navigate = useNavigate();
  const { id } = useParams();
  const { time } = useTimer({
    autostart: true,
    endTime: CREATING_TIME,
    onTimeOver: () => {
      navigate(`/coffeeReady/${id}`);
    },
  });
  const handleNavigateToNextScreen = () => {
    navigate(`/coffeeReady/${id}`);
  }
  const completedPercent = Math.ceil((time / CREATING_TIME) * 100)

  return (
    <CenterContainer>
      <div><CoffeeCreatingText>{`Ваше кофе готово на: ${completedPercent}%`}</CoffeeCreatingText></div>
      <CoffeeCreatingProgressBarContainer>
        <ProgressBar
          bgColor="#F5D009"
          isLabelVisible={false}
          completed={time}
          maxCompleted={CREATING_TIME}
        />
      </CoffeeCreatingProgressBarContainer>
      <Button onClick={handleNavigateToNextScreen}>Для быстрого перехода на другую страницу</Button>
    </CenterContainer>
  );
};
