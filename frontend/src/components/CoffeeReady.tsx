import React, { useEffect } from "react";
import { Emulator } from "../emulator";
import { useParams } from "react-router-dom";
import { CenterContainer, InstructionText, Title2 } from './styles'

export const CoffeeReady = () => {

  const { id } = useParams();
  useEffect(() => {
    const handleCoffeeReady = (isCoffeeReady: boolean) => {
      if (isCoffeeReady) {
        alert(`Клиент получил напиток номер ${id}!`);
      } else {
        alert("Клиент не получил напиток!");
      }
    };
    const emulator = new Emulator();
    if (id) {
      emulator.Vend(Number(id), handleCoffeeReady);
    }
    return () => {
      emulator.VendCancel();
    };
  }, [id]);

  return (
    <CenterContainer>
      <Title2>Кофе готово</Title2>
      <ul>
        <InstructionText>Для эмуляции успешной выдачи - нажмите Y</InstructionText>
        <InstructionText>Для эмуляции не выдачи кофе - нажмите N</InstructionText>
      </ul>
    </CenterContainer>
  );
};
