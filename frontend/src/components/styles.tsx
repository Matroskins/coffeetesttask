import styled, { css } from "styled-components";

const fontFamily = css`
  font-family: 'Mont';
`
const fontWeightTitle = css`
  font-weight: 400;
`
const flexCenter = css`
  display: flex;
  justify-content: center;
  align-items: center;
`
const flexColumn = css`
  display: flex;
  flex-direction: column;
`;

const standartTextSize = css`
  font-size: 5.2rem;
`
export const DrinksContainer = styled.div`
  background-color: #efccb9;
  width: 100%;
  height: 100%;
`;

export const DrinksListContainer = styled.div`
  background-color: #FFFFFF;
  height: 100%;
  border-radius: 16px;
  
`;
export const DrinksHeaderTitle = styled.h1`
  font-size: 6.4rem;
  margin-block-start: 0;
  ${fontWeightTitle};
`;

export const Title2 = styled.h2<{ center?: boolean }>`
  font-size: 8rem;
  text-align: ${props => props.center ? "center" : "start"};
  ${fontWeightTitle};
`;

export const DrinkContainer = styled.div`
  border: 1px solid #d8d8d8;
  cursor: pointer;
  justify-content: space-between;
  overflow: auto;
  align-items: center;
  border-radius: 40px;
  min-width: 333px;
  max-width: 450px;
  min-height: 500px;
  ${flexColumn}
  &:hover {
    opacity: 0.4;
  }
`;

export const DrinkDescriptionText = styled.div`
  font-size: 4rem;
  ${flexColumn}

`;
export const DrinkName = styled.span`
  text-align: center;
`;
export const DrinkPrice = styled.span`
  font-size: 6.4rem;
  font-weight: 600;
`;

export const DrinkListContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 16px;
  justify-content: flex-start;
  padding-left: 16px;
  padding-right: 16px;
  margin-bottom: 16px;
`;

export const ChoosePaymentContainer = styled.div`
  height: 100%;
  justify-content: space-between;
  align-items: center;
  ${flexColumn}
`;

export const ControlsContainer = styled.div`
  display: flex;
  justify-content: space-between;
  gap: 32px;
`;

export const CloseModalButton = styled.button`
  background-color: #FAFAFA;
  border: 1px solid #D3D3D3;
  cursor: pointer;
  border-radius: 10px;
  height: 120px;
  width: 240px;
`;


export const ChoosePaymentDescriptionCurrency = styled.div`
  width: 238px;
  height: 238px;
  background-color: #F5D009;
  border-radius: 50px;
  ${flexCenter}
`;
export const ChoosePaymentDescription = styled.h2`
  text-align: center;
  ${standartTextSize};
  ${fontWeightTitle}
`;

export const Button = styled.button`
  font-size: 3.6rem;
  border: none;
  padding: 8px 16px;
  border-radius: 8px;
  background-color: #F3F3F3;
  &:not(:disabled){
  &:hover{ 
      background-color: #F5D009; 
  }
}
  &:disabled {
    cursor: not-allowed;
  }
  ${fontFamily};
`;

export const CashPaymentContainer = styled.div`
  align-items: center;
  justify-content: space-around;
  ${flexColumn};
`;


export const InstructionText = styled.li`
  ${standartTextSize}
  ${fontFamily}`
  ;


export const CenterContainer = styled.div`
  flex-direction: column;
  ${flexCenter}
`;

export const CoffeeCreatingProgressBarContainer = styled.div`
  margin-top: 24px;
  margin-bottom: 24px;
  width: 160px;
`;

export const CoffeeCreatingText = styled.span`
  ${standartTextSize}
  ${fontFamily}`
  ;