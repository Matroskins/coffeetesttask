export type TCardPayStep =
  | "Приложите карту"
  | "Обработка карты"
  | "Связь c банком";
export type TBankCardPurchase = (
  amount: number,
  cb: (isFinished: boolean) => void,
  display_cb: (step: TCardPayStep) => void,
) => void;
export type TEmulator = {
  StartCashin: (cb: (amount: number) => void) => void;
  StopCashin: () => void;
  BankCardCancel: () => void;
  emulatorCashListener?: void;
};
export type TPayType = "CARD" | "CASH";

export type TDrinkView = {
  name: string;
  price: number;
  id: number;
  imgName: string;
  onSelectDrink: (id: number) => void;
};


export type TDrinkImage = {
  name: string;
  imgName: string;
}

export type TChoosePaymentFormView = {
  onPay: (selectedPayType: TPayType) => void;
  onCloseModal: () => void;
};